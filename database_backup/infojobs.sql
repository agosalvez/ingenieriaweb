-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 26-01-2017 a las 20:17:40
-- Versión del servidor: 5.7.17-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `infojobs`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id`, `user`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria-empresa`
--

CREATE TABLE `categoria-empresa` (
  `id` int(11) NOT NULL,
  `tipo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria-empresa`
--

INSERT INTO `categoria-empresa` (`id`, `tipo`) VALUES
(1, 'Informática'),
(2, 'Industria'),
(3, 'Agrícola'),
(4, 'Transportes'),
(5, 'Mercancias'),
(6, 'Seguridad'),
(7, 'Construcción'),
(8, 'Electrónica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cv`
--

CREATE TABLE `cv` (
  `id` int(11) NOT NULL,
  `idTrabajador` int(11) DEFAULT NULL,
  `estudios` text NOT NULL,
  `idiomas` text NOT NULL,
  `conocimientos` text NOT NULL,
  `otrosDatos` text NOT NULL,
  `preferencias` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id` int(11) NOT NULL,
  `dni` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `poblacion` varchar(255) NOT NULL,
  `provincia` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `trabajadores` int(11) NOT NULL,
  `numOfertas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id`, `dni`, `password`, `nombre`, `direccion`, `telefono`, `poblacion`, `provincia`, `email`, `trabajadores`, `numOfertas`) VALUES
(1, '98765432Z', '098f6bcd4621d373cade4e832627b4f6', 'Everis SL', 'Calle de mi empresa, 123', '965323232', 'Alicante', 'Alicante', 'info@empresa1.es', 50, 0),
(2, '65498715B', '098f6bcd4621d373cade4e832627b4f6', 'Pccomponentes SL', 'Calle de alhama, 2', '958747474', 'Alhama de murcia', 'Murcia', 'pccomponentes@ya.es', 60, 0),
(3, 'test', '098f6bcd4621d373cade4e832627b4f6', 'Gax Creaciones SL', 'calle denia, 34', '965323232', 'Dénia', 'Alicante', 'info@gaxcreaciones.es', 10, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `experiencia-cv`
--

CREATE TABLE `experiencia-cv` (
  `id` int(11) NOT NULL,
  `empresa` varchar(255) NOT NULL,
  `duracion` int(11) NOT NULL,
  `salario` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas`
--

CREATE TABLE `idiomas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(244) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `idiomas`
--

INSERT INTO `idiomas` (`id`, `nombre`) VALUES
(1, 'Español'),
(2, 'Polaco'),
(3, 'Inglés'),
(4, 'Francés'),
(5, 'Italiano'),
(6, 'Chino'),
(7, 'Japonés'),
(8, 'Valenciano'),
(9, 'Catalán'),
(10, 'Euskera'),
(11, 'Gallego'),
(12, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidades`
--

CREATE TABLE `localidades` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `localidades`
--

INSERT INTO `localidades` (`id`, `nombre`) VALUES
(1, 'Alicante'),
(2, 'Valencia'),
(6, 'Castellón'),
(7, 'Murcia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel-trabajador`
--

CREATE TABLE `nivel-trabajador` (
  `id` int(11) NOT NULL,
  `nivel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `nombre`) VALUES
(1, 'noticia 1'),
(2, 'noticia 2'),
(3, 'noticia 3'),
(4, 'noticia 4'),
(5, 'noticia 5'),
(6, 'noticia 6'),
(7, 'noticia 7'),
(8, 'noticia 8'),
(9, 'noticia 9'),
(10, 'noticia 10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oferta`
--

CREATE TABLE `oferta` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `idEmpresa` int(11) DEFAULT NULL,
  `estudiosMinimos` varchar(255) NOT NULL,
  `ciudad` varchar(255) NOT NULL,
  `conocimientos` varchar(1000) NOT NULL,
  `reqMin` varchar(255) NOT NULL,
  `reqMax` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `referencia` varchar(500) NOT NULL,
  `departamento` varchar(255) NOT NULL,
  `personalACargo` int(3) NOT NULL,
  `vacantes` int(3) NOT NULL,
  `duracion` varchar(255) NOT NULL,
  `jornada` varchar(255) NOT NULL,
  `salario` varchar(255) NOT NULL,
  `inscritos` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `oferta`
--

INSERT INTO `oferta` (`id`, `titulo`, `idEmpresa`, `estudiosMinimos`, `ciudad`, `conocimientos`, `reqMin`, `reqMax`, `descripcion`, `referencia`, `departamento`, `personalACargo`, `vacantes`, `duracion`, `jornada`, `salario`, `inscritos`) VALUES
(1, 'Analista programador', 3, 'Ciclo Formativo Grado Superior', 'Alicante (Alicante)', 'COBOL  DB2  Pruebas  Telecomunicaciones ', '2 años de experiencia en puesto similar.\r\n', '* Disponibilidad de incorporación inmediata al proyecto.\r\n', 'Sandav consultores es una empresa en pleno proceso de expansión que nació tras una dilatada experiencia en el sector de las tecnologías de la información en clientes y proyectos de todos los sectores.\r\nOfrecemos soluciones de consultoría de negocios, gestión de proyectos, análisis de sistemas, desarrollos a medida, contando con profesionales altamente capacitados. \r\nTambién ofrecemos una amplia gama de servicios flexibles destinados a ofrecer una solución integral a las necesidades de las empresas, con el fin de que las mismas puedan dedicar todo su esfuerzo a su actividad principal.\r\nActualmente desarrollamos nuestra actividad cubriendo los siguientes sectores: Telecomunicaciones, Banca y Seguros, Administración Pública e Industria-Transporte-Energía.\r\n¿Qué ofrecemos?\r\nContrataciones indefinidas, atractiva carrera profesional en función de la experiencia y potencial de futuro, y diversidad de proyectos punteros tanto a nivel nacional como internacional. \r\n¿Por qué elegirnos? \r\nPorque:\r\n- Combinamos lo mejor de la gran empresa (oportunidades y desarrollo) y lo mejor de la pequeña (cercanía y flexibilidad).\r\n- Ofrecemos servicios de innovación exclusivos y muy próximos al cliente.\r\n- Participamos en todas las etapas del ciclo productivo, desde la fase de I+D hasta el servicio postventa.\r\n\r\nPara importante cliente en Barcelona buscamos un Programador Host con experiencia en Cobol y en DB2.\r\n\r\n* Funciones del puesto:\r\n- Desarrollo de programas\r\n- Pruebas unitarias.\r\n\r\nSe trata de un proyecto temporal con posibilidades reales de continuidad.\r\n\r\n* Salario acorde a la valía del candidato.\r\n\r\n* Muy valorable disponibilidad inmediata de incorporación al proyecto.\r\nSe trata de un proyecto estable y un salario acorde a la valía del candidato.', '', 'Informática y telecomunicaciones - Programación', 0, 1, 'Contrato 1 año', 'Jornada completa', '25000', 1),
(2, 'Programador senior', 3, 'Formación Profesional Grado Superior - Informática', 'Huesca capital', 'Conocimientos Weblogic, Jboss,Apache Tomcat. ', 'Al menos 1 año', 'Experiencia en Sistemas Informáticos', '<p>\r\n	Desarrollo de Medios y Sistemas -DMS- precisa incorporar a un Asistente Inform&aacute;tico para importante proyecto de Huesca. Se ofrece incorporaci&oacute;n inmediata en proyecto muy estable.</p>\r\n', '', 'Servicios y tecnología de la información', 0, 1, 'indefinido, jornada completa', 'Media jornada', 'Salario no disponible', 1),
(3, '2Analista programador', 2, 'Ciclo Formativo Grado Superior', 'Alicante (Alicante)', 'COBOL  DB2  Pruebas  Telecomunicaciones ', '2 años de experiencia en puesto similar.\r\n', '* Disponibilidad de incorporación inmediata al proyecto.\r\n', 'Sandav consultores es una empresa en pleno proceso de expansión que nació tras una dilatada experiencia en el sector de las tecnologías de la información en clientes y proyectos de todos los sectores.\r\nOfrecemos soluciones de consultoría de negocios, gestión de proyectos, análisis de sistemas, desarrollos a medida, contando con profesionales altamente capacitados. \r\nTambién ofrecemos una amplia gama de servicios flexibles destinados a ofrecer una solución integral a las necesidades de las empresas, con el fin de que las mismas puedan dedicar todo su esfuerzo a su actividad principal.\r\nActualmente desarrollamos nuestra actividad cubriendo los siguientes sectores: Telecomunicaciones, Banca y Seguros, Administración Pública e Industria-Transporte-Energía.\r\n¿Qué ofrecemos?\r\nContrataciones indefinidas, atractiva carrera profesional en función de la experiencia y potencial de futuro, y diversidad de proyectos punteros tanto a nivel nacional como internacional. \r\n¿Por qué elegirnos? \r\nPorque:\r\n- Combinamos lo mejor de la gran empresa (oportunidades y desarrollo) y lo mejor de la pequeña (cercanía y flexibilidad).\r\n- Ofrecemos servicios de innovación exclusivos y muy próximos al cliente.\r\n- Participamos en todas las etapas del ciclo productivo, desde la fase de I+D hasta el servicio postventa.\r\n\r\nPara importante cliente en Barcelona buscamos un Programador Host con experiencia en Cobol y en DB2.\r\n\r\n* Funciones del puesto:\r\n- Desarrollo de programas\r\n- Pruebas unitarias.\r\n\r\nSe trata de un proyecto temporal con posibilidades reales de continuidad.\r\n\r\n* Salario acorde a la valía del candidato.\r\n\r\n* Muy valorable disponibilidad inmediata de incorporación al proyecto.\r\nSe trata de un proyecto estable y un salario acorde a la valía del candidato.', '', '2Informática y telecomunicaciones - Programación', 0, 1, 'Contrato 1 año', 'Jornada completa', '25000', 0),
(4, '2Programador senior', 2, 'Formación Profesional Grado Superior - Informática', 'Huesca capital', 'Conocimientos Weblogic, Jboss,Apache Tomcat. ', 'Al menos 1 año', 'Experiencia en Sistemas Informáticos', '<p>\r\n	Desarrollo de Medios y Sistemas -DMS- precisa incorporar a un Asistente Inform&aacute;tico para importante proyecto de Huesca. Se ofrece incorporaci&oacute;n inmediata en proyecto muy estable.</p>\r\n', '', 'Servicios y tecnología de la información', 0, 1, 'indefinido, jornada completa', 'Media jornada', 'Salario no disponible', 0),
(5, '3Analista programador', 2, 'Ciclo Formativo Grado Superior', 'Alicante (Alicante)', 'COBOL  DB2  Pruebas  Telecomunicaciones ', '2 años de experiencia en puesto similar.\r\n', '* Disponibilidad de incorporación inmediata al proyecto.\r\n', 'Sandav consultores es una empresa en pleno proceso de expansión que nació tras una dilatada experiencia en el sector de las tecnologías de la información en clientes y proyectos de todos los sectores.\r\nOfrecemos soluciones de consultoría de negocios, gestión de proyectos, análisis de sistemas, desarrollos a medida, contando con profesionales altamente capacitados. \r\nTambién ofrecemos una amplia gama de servicios flexibles destinados a ofrecer una solución integral a las necesidades de las empresas, con el fin de que las mismas puedan dedicar todo su esfuerzo a su actividad principal.\r\nActualmente desarrollamos nuestra actividad cubriendo los siguientes sectores: Telecomunicaciones, Banca y Seguros, Administración Pública e Industria-Transporte-Energía.\r\n¿Qué ofrecemos?\r\nContrataciones indefinidas, atractiva carrera profesional en función de la experiencia y potencial de futuro, y diversidad de proyectos punteros tanto a nivel nacional como internacional. \r\n¿Por qué elegirnos? \r\nPorque:\r\n- Combinamos lo mejor de la gran empresa (oportunidades y desarrollo) y lo mejor de la pequeña (cercanía y flexibilidad).\r\n- Ofrecemos servicios de innovación exclusivos y muy próximos al cliente.\r\n- Participamos en todas las etapas del ciclo productivo, desde la fase de I+D hasta el servicio postventa.\r\n\r\nPara importante cliente en Barcelona buscamos un Programador Host con experiencia en Cobol y en DB2.\r\n\r\n* Funciones del puesto:\r\n- Desarrollo de programas\r\n- Pruebas unitarias.\r\n\r\nSe trata de un proyecto temporal con posibilidades reales de continuidad.\r\n\r\n* Salario acorde a la valía del candidato.\r\n\r\n* Muy valorable disponibilidad inmediata de incorporación al proyecto.\r\nSe trata de un proyecto estable y un salario acorde a la valía del candidato.', '', 'Informática y telecomunicaciones - Programación', 0, 1, 'Contrato 1 año', 'Jornada completa', '25000', 0),
(6, '3Programador senior', 2, 'Formación Profesional Grado Superior - Informática', 'Huesca capital', 'Conocimientos Weblogic, Jboss,Apache Tomcat. ', 'Al menos 1 año', 'Experiencia en Sistemas Informáticos', '<p>\r\n	Desarrollo de Medios y Sistemas -DMS- precisa incorporar a un Asistente Inform&aacute;tico para importante proyecto de Huesca. Se ofrece incorporaci&oacute;n inmediata en proyecto muy estable.</p>\r\n', '', 'Servicios y tecnología de la información', 0, 1, 'indefinido, jornada completa', 'Media jornada', 'Salario no disponible', 0),
(7, '4Analista programador', 2, 'Ciclo Formativo Grado Superior', 'Alicante (Alicante)', 'COBOL  DB2  Pruebas  Telecomunicaciones ', '2 años de experiencia en puesto similar.\r\n', '* Disponibilidad de incorporación inmediata al proyecto.\r\n', 'Sandav consultores es una empresa en pleno proceso de expansión que nació tras una dilatada experiencia en el sector de las tecnologías de la información en clientes y proyectos de todos los sectores.\r\nOfrecemos soluciones de consultoría de negocios, gestión de proyectos, análisis de sistemas, desarrollos a medida, contando con profesionales altamente capacitados. \r\nTambién ofrecemos una amplia gama de servicios flexibles destinados a ofrecer una solución integral a las necesidades de las empresas, con el fin de que las mismas puedan dedicar todo su esfuerzo a su actividad principal.\r\nActualmente desarrollamos nuestra actividad cubriendo los siguientes sectores: Telecomunicaciones, Banca y Seguros, Administración Pública e Industria-Transporte-Energía.\r\n¿Qué ofrecemos?\r\nContrataciones indefinidas, atractiva carrera profesional en función de la experiencia y potencial de futuro, y diversidad de proyectos punteros tanto a nivel nacional como internacional. \r\n¿Por qué elegirnos? \r\nPorque:\r\n- Combinamos lo mejor de la gran empresa (oportunidades y desarrollo) y lo mejor de la pequeña (cercanía y flexibilidad).\r\n- Ofrecemos servicios de innovación exclusivos y muy próximos al cliente.\r\n- Participamos en todas las etapas del ciclo productivo, desde la fase de I+D hasta el servicio postventa.\r\n\r\nPara importante cliente en Barcelona buscamos un Programador Host con experiencia en Cobol y en DB2.\r\n\r\n* Funciones del puesto:\r\n- Desarrollo de programas\r\n- Pruebas unitarias.\r\n\r\nSe trata de un proyecto temporal con posibilidades reales de continuidad.\r\n\r\n* Salario acorde a la valía del candidato.\r\n\r\n* Muy valorable disponibilidad inmediata de incorporación al proyecto.\r\nSe trata de un proyecto estable y un salario acorde a la valía del candidato.', '', '2Informática y telecomunicaciones - Programación', 0, 1, 'Contrato 1 año', 'Jornada completa', '25000', 0),
(8, '4Programador senior', 2, 'Formación Profesional Grado Superior - Informática', 'Huesca capital', 'Conocimientos Weblogic, Jboss,Apache Tomcat. ', 'Al menos 1 año', 'Experiencia en Sistemas Informáticos', '<p>\r\n	Desarrollo de Medios y Sistemas -DMS- precisa incorporar a un Asistente Inform&aacute;tico para importante proyecto de Huesca. Se ofrece incorporaci&oacute;n inmediata en proyecto muy estable.</p>\r\n', '', 'Servicios y tecnología de la información', 0, 1, 'indefinido, jornada completa', 'Media jornada', 'Salario no disponible', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`id`, `nombre`) VALUES
(1, 'España'),
(2, 'Polonia'),
(3, 'Francia'),
(4, 'UK'),
(5, 'Italia'),
(6, 'Alemania'),
(7, 'Portugal'),
(8, 'Holanda');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador`
--

CREATE TABLE `trabajador` (
  `id` int(11) NOT NULL,
  `dni` varchar(255) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `genero` tinyint(1) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `telefono2` varchar(255) DEFAULT NULL,
  `poblacion` varchar(255) DEFAULT NULL,
  `provincia` varchar(255) DEFAULT NULL,
  `pais` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `anyosExperiencia` int(11) DEFAULT NULL,
  `cartaPresentacion` text,
  `password` varchar(255) DEFAULT NULL,
  `ofertasInscritas` int(11) DEFAULT '0',
  `cv` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `trabajador`
--

INSERT INTO `trabajador` (`id`, `dni`, `nombre`, `apellidos`, `genero`, `direccion`, `telefono`, `telefono2`, `poblacion`, `provincia`, `pais`, `email`, `dob`, `anyosExperiencia`, `cartaPresentacion`, `password`, `ofertasInscritas`, `cv`) VALUES
(1, '12345678A', 'Adrián', 'Gosálvez', 1, 'mi calle, 23', '965323232', '654212121', 'Alicante', 'Alicante', 'España', 'test', NULL, 5, 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.', '098f6bcd4621d373cade4e832627b4f6', 0, 0),
(39, '32132121A', 'Jorge', 'Segovia', 1, 'calle en elche, 23', NULL, NULL, 'Elche', 'Alicante', 'España', 'test2', '1995-12-01', 2, NULL, 'ad0234829205b9033196ba818f7a872b', 0, 0),
(41, '12398734H', 'prueba2', 'apellidos 2', 0, 'asdrfasda', NULL, NULL, '-1', '-1', '-1', '123', '0000-00-00', 0, '', '4297f44b13955235245b2497399d7a93', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajadorOferta`
--

CREATE TABLE `trabajadorOferta` (
  `idTrabajador` int(11) UNSIGNED NOT NULL,
  `idOferta` int(11) NOT NULL,
  `aceptado` tinyint(1) NOT NULL DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `trabajadorOferta`
--

INSERT INTO `trabajadorOferta` (`idTrabajador`, `idOferta`, `aceptado`) VALUES
(1, 1, -1),
(41, 1, -1),
(41, 2, -1);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_prueba`
--
CREATE TABLE `vista_prueba` (
`id` int(11)
,`titulo` varchar(255)
,`idEmpresa` int(11)
,`estudiosMinimos` varchar(255)
,`ciudad` varchar(255)
,`conocimientos` varchar(1000)
,`reqMin` varchar(255)
,`reqMax` varchar(255)
,`descripcion` text
,`referencia` varchar(500)
,`departamento` varchar(255)
,`personalACargo` int(3)
,`vacantes` int(3)
,`duracion` varchar(255)
,`jornada` varchar(255)
,`salario` varchar(255)
,`inscritos` int(11) unsigned
);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_prueba`
--
DROP TABLE IF EXISTS `vista_prueba`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_prueba`  AS  select `oferta`.`id` AS `id`,`oferta`.`titulo` AS `titulo`,`oferta`.`idEmpresa` AS `idEmpresa`,`oferta`.`estudiosMinimos` AS `estudiosMinimos`,`oferta`.`ciudad` AS `ciudad`,`oferta`.`conocimientos` AS `conocimientos`,`oferta`.`reqMin` AS `reqMin`,`oferta`.`reqMax` AS `reqMax`,`oferta`.`descripcion` AS `descripcion`,`oferta`.`referencia` AS `referencia`,`oferta`.`departamento` AS `departamento`,`oferta`.`personalACargo` AS `personalACargo`,`oferta`.`vacantes` AS `vacantes`,`oferta`.`duracion` AS `duracion`,`oferta`.`jornada` AS `jornada`,`oferta`.`salario` AS `salario`,`oferta`.`inscritos` AS `inscritos` from `oferta` ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin` (`user`);

--
-- Indices de la tabla `categoria-empresa`
--
ALTER TABLE `categoria-empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cv`
--
ALTER TABLE `cv`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `experiencia-cv`
--
ALTER TABLE `experiencia-cv`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `idiomas`
--
ALTER TABLE `idiomas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `localidades`
--
ALTER TABLE `localidades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nivel-trabajador`
--
ALTER TABLE `nivel-trabajador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oferta`
--
ALTER TABLE `oferta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dni` (`dni`);

--
-- Indices de la tabla `trabajadorOferta`
--
ALTER TABLE `trabajadorOferta`
  ADD PRIMARY KEY (`idTrabajador`,`idOferta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `categoria-empresa`
--
ALTER TABLE `categoria-empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `cv`
--
ALTER TABLE `cv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `experiencia-cv`
--
ALTER TABLE `experiencia-cv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `idiomas`
--
ALTER TABLE `idiomas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `localidades`
--
ALTER TABLE `localidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `nivel-trabajador`
--
ALTER TABLE `nivel-trabajador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `oferta`
--
ALTER TABLE `oferta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
