<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Private_model extends CI_Model {
	/*
	 * Función que carga los datos del usuario
	 */
	public function getUserData($email) {
        $query = 'SELECT *
                    FROM trabajador 
                  WHERE 1
                      AND email = "' . $email . '" 
                  ';
        $res = $this->db->query($query);
        return ($res->num_rows() > 0) ? $res->row_array() : array();
    }
    /*
     *    Obtengo todos los datos del trabajador por su id
     */
    public function getUserDataFromId($id) {
        $query = 'SELECT *
                    FROM trabajador 
                  WHERE 1
                      AND id = "' . $id . '" 
                  ';
        $res = $this->db->query($query);
        return ($res->num_rows() > 0) ? $res->row_array() : array();
    }
    /*
     *    Obtengo id del usuario por su email
     */
    public function getIdUserFromEmail($email) {
        $query = 'SELECT id
                    FROM trabajador 
                  WHERE 1
                      AND email = "' . $email . '" 
                  ';
        $res = $this->db->query($query);
        return ($res->num_rows() > 0) ? $res->row_array()['id'] : -1;
    }
    /*
     *    Incribo un usuario a una oferta de trabajo
     */
    public function inscribirUsuarioAOferta($idOferta, $idTrabajador) {
        try {
            $res = $this->db->query('INSERT IGNORE INTO `trabajadorOferta` (idTrabajador, idOferta) VALUES (' . $idTrabajador . ',' . $idOferta . ')');
            if ($this->db->affected_rows()) {
                // Incremento numero de inscritos en la oferta
                $this->db->set('inscritos', 'inscritos+1', FALSE);
                $this->db->where('id', $idOferta);
                $this->db->update('oferta');
            }
        } catch (Exception $e) {
            echo $e->getMessage(), "\n";
        }
    }
    /*
     *    Función que desinscribe a un usuario de una oferta de trabajo
     */
    public function desinscribirUsuarioAOferta($idOferta, $idTrabajador) {
        try {
            $where = array('idTrabajador' => $idTrabajador,
                            'idOferta' => $idOferta
                            );
            $this->db->delete('trabajadorOferta', $where);

            // Decremento numero de inscritos en la oferta
            $this->db->set('inscritos', 'inscritos-1', FALSE);
            $this->db->where('id', $idOferta);
            $this->db->update('oferta');
        } catch (Exception $e) {
            echo $e->getMessage(), "\n";
        }
    }
    /*
     *    Función que obtiene las ofertas asignadas a un trabajador
     */
    public function getOffersSigned($emailUser) {
        $query = 'SELECT GROUP_CONCAT(idOferta SEPARATOR \',\') AS ofertas
                    FROM trabajador t
                    LEFT JOIN trabajadorOferta on t.id = idTrabajador
                    WHERE email = "' . $emailUser . '"
              ';
        $res = $this->db->query($query);
        return ($res->num_rows() > 0) ? $res->row_array() : array();
    }

    public function checkUserSignedOferta($idOferta, $userId) {
        $query = 'SELECT COUNT(*) as total
                    FROM trabajadorOferta t
                    WHERE 1
                    AND idTrabajador = ' . $userId . '
                    AND idOferta = '. $idOferta
                  ;
        $res = $this->db->query($query);
        foreach ($res->result() as $row) {
            $res = $row->total;
        }

        return ($res) ? true : false;
    }
}