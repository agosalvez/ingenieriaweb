<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Public_model extends CI_Model {
	/*
	 * Función que hace login para la parte pública
	 */
	public function doLogin($array) {
	    $query = 'SELECT COUNT(*) as total
                    FROM trabajador 
                  WHERE 1
                      AND email = "' . $array['email'] . '" 
                      AND password = MD5("' . $array['password'] . '")
                  ';
        $res = $this->db->query($query);
        foreach ($res->result() as $row) {
            $res = $row->total;
        }

        return ($res) ? true : false;
	}

    public function getAllData($table) {
        $query = $this->db->get($table);
        return ( $query->num_rows() > 0 ) ? $query->result() : array();
    }

    public function getAllOfertas($per_page, $cad = '') {
	    $where  = ($cad != '') ? ' AND o.titulo LIKE "%' . $cad . '%" ' : '' ;
	    $aux    = ($this->uri->segment(3) != '') ? $this->uri->segment(3) . ', ' : '';
	    $query  = 'SELECT e.nombre as empresa, o.* 
                    FROM oferta o
                      LEFT JOIN empresa e ON e.id = o.idEmpresa
                      WHERE 1
                      ' . $where . '
                      ORDER BY o.id
                      LIMIT ' . $aux . $per_page
                    ;
        //var_dump($query);die;
        $query = $this->db->query($query);
        return ( $query->num_rows() > 0 ) ? $query->result_array() : array();
    }

    /*
     * Devuelve los datos de una ofeta a partir de su id
    */
    public function getOferta($idOferta) {
        $query = 'SELECT o.*, e.nombre as empresa, e.provincia as empresaProvincia
                    FROM oferta o
                    LEFT JOIN empresa e ON o.idEmpresa = e.id
                    WHERE o.id = ' . $idOferta
                    ;
        $query = $this->db->query($query);
        return ( $query->num_rows() > 0 ) ? $query->result_array()[0] : array();
    }

    public function numOfertas() {
	    if($this->session->has_userdata('buscar')) {
            $this->db->like('titulo', $this->session->userdata('buscar'));
        }
        return $this->db->get('oferta')->num_rows();
    }

    public function registro($data) {
        return $this->db->insert('trabajador', $data);
    }

    public function userUploadCv($userId) {
        $this->db->set('cv', 1, FALSE);
        $this->db->where('id', $userId);

        return $this->db->update('trabajador');
    }
}














