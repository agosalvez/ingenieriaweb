<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa_model extends CI_Model {

	/*
	 * Función que hace login para el backoffice
	 */
	public function doLogin($array) {
	    $query = 'SELECT COUNT(*) as total
                    FROM empresa 
                  WHERE 1
                      AND dni = "' . $array['user'] . '" 
                      AND password = MD5("' . $array['password'] . '")
                  ';
        $res = $this->db->query($query);
        foreach ($res->result() as $row) {
            $res = $row->total;
        }

        return ($res) ? true : false;
	}

	public function getIdEmpresa($name) {
	    $query = 'SELECT id
	                FROM empresa
	                WHERE dni = "' . $name . '"
                ';
        $res = $this->db->query($query);
        return ( $res->num_rows() > 0 ) ? $res->result_array()[0]['id'] : 0;
    }

    public function getCandidatos($idEmpresa) {
        $query = 'SELECT o.titulo, tr.idTrabajador AS idT, tr.idOferta AS idO, t.*
                    FROM trabajadorOferta tr
                    LEFT JOIN trabajador t ON tr.idTrabajador = t.id
                    LEFT JOIN oferta o ON o.id = tr.idOferta
                    WHERE 1
                    AND o.idEmpresa = ' . $idEmpresa . '
                    AND tr.aceptado = -1';
        $query = $this->db->query($query);
        return ( $query->num_rows() > 0 ) ? $query->result_array() : array();
    }

    public function setCandidatura($data) {
        if ($data['type'] == 'aceptar') {
            $this->db->set('aceptado', 1, FALSE);
        } else if ($data['type'] == 'rechazar') {
            $this->db->set('aceptado', 0, FALSE);
        }
        $this->db->where('idTrabajador', $data['idTrabajador']);
        $this->db->where('idOferta', $data['idOferta']);

        return $this->db->update('trabajadorOferta');
    }
}