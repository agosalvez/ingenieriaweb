<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('recortarDescripcion'))
{
    function recortarDescripcion($data = '', $tam = 150, $tag = '...')
    {
        if ($data != '') {
            for ($i = 0; $i < count($data['ofertas']); ++$i) {
                $data['ofertas'][$i]['descripcion'] = substr($data['ofertas'][$i]['descripcion'], 0, $tam) . $tag;
            }
            return $data;
        } else {
            return $var;
        }
    }
}

if ( ! function_exists('checkOffersSignedButton'))
{
    function checkOffersSignedButton($listaOfertasUser = '', $ofertasInscritas) {
        $trenOfertas = explode(',', $ofertasInscritas['ofertas']);

        for ($i = 0; $i < count($listaOfertasUser); ++$i) {
            if (in_array($listaOfertasUser[$i]['id'], $trenOfertas)) {
                $listaOfertasUser[$i]['inscrito'] = 1;
            } else {
                $listaOfertasUser[$i]['inscrito'] = 0;
            }
        }
        return $listaOfertasUser;
    }
}
