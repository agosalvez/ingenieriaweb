<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<li itemscope="" itemtype="http://schema.org/JobPosting" id="of_f549c973b74b2aafe282144949ccb6" class="item    vhpz" value="0">
    <meta itemprop="datePosted" content="2017/01/17">
    <div class="content-top">
        <div class="content-type-text">
            <h3 class="job-list-title">
                <a href="<?php echo base_url() . 'index.php/home/detalleOferta/' . $id ?>" class="lines-2 clamp" itemprop="url">
                    <span class="titulo-oferta"><?=$titulo?></span>
                </a>
            </h3>
            <h4 class="job-list-subtitle">
                <span class="ellipsis">
                    <span itemprop="name"><?=$empresa?></span>
                </span>
            </h4>
            <ul class="tag-group">
                <li class="tag-divider tag-ellipsis-cols2">
                    <span class="ellipsis" itemprop="jobLocation"><?=$ciudad?></span></li>
                <li>
                    <span class="marked">Hace 1 dia</span>
                    <span class="sticker sticker-marked sticker-small">Nueva</span>
                </li>
            </ul>
            <p class="descripcion-oferta"><span><?=$descripcion?></span></p>
            <ul class="tag-group hide-small-device">
                <li class="tag-divider" itemprop="employmentType"><?=$duracion?></li>
                <li class="tag-divider" itemprop="employmentType"><?=$jornada?></li>
                <li itemprop="baseSalary"><?=$salario?>€ brutos/año</li>
                <li itemprop="baseSalary"><?=$inscritos?> persona/s inscrita/s</li>
            </ul>
        </div>
        <div class="content-type-media">
        </div>
    </div>
    <?php
    if ($this->session->has_userdata('user-public')) {
        if ($inscrito) {
            echo anchor("home/desinscribirOferta/" . $id . '/' . str_replace('/', '837599239592', $this->uri->uri_string()), "Desinscribirme", array("title" => "Desinscribirme", "class" => "btn btn-info", "style" => "float:right"));
        } else {
            echo anchor("home/inscribirOferta/" . $id . '/' . str_replace('/', '837599239592', $this->uri->uri_string()), "Inscribirme", array("title" => "Inscribirme", "class" => "btn btn-primary", "style" => "float:right"));
        }
    } else {
        echo anchor("home/login", "Inscribirme", array("title" => "Inscribirme", "class" => "btn btn-primary", "style" => "float:right"));
    }
    ?>
</li>
<hr>