<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="col-sm-12 panel">
    <ul id="offer-list" class="job-list">
        <?php
        for($i = 0; $i < count($ofertas); ++$i) {
            $this->load->view('home/vista-oferta', $ofertas[$i]);
        }
        ?>
    </ul>
    <?php echo $paginacion; ?>
</div>
