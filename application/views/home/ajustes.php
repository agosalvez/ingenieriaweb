<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php

?>
<!DOCTYPE html>
<html>
<head>
    <title>Infojobs - Mi perfil</title>
    <?php $this->load->view('includes/header.php');?>
</head>

<body>
    <header class="container" align="center">
        <div class="container">
            <h3>Mi perfil</h3>
        </div>
    </header>
    <main class="container">
        <div class="panel-group panel-default col-md-12">
        <div class="panel-heading">
            <h2><?=$userData['email']?></h2>
            <div class="panel-body" style="background-color: #f5f5f0">
                <div class="col-md-3">
                    <img src="<?php echo base_url() ?>assets/images/Icon-user.png" class="img-responsive img-circle">
                </div>
                <div class="col-md-offset-3">
                    <table>
                        <tbody>
                            <tr>
                                <td><label id="lblNombre" class="control-label">Nombre:</label></td>
                                <td><?=$userData['nombre']?></td>
                            </tr>
                            <tr>
                                <td><label id="lblApellidos" class="control-label">Apellidos:</label></td>
                                <td><?=$userData['apellidos']?></td>
                            </tr>
                            <tr>
                                <td><label id="lblDni" class="control-label">Dni:</label></td>
                                <td><?=$userData['dni']?></td>
                            </tr>
                            <tr>
                                <td><label id="lblDireccion" class="control-label">Dirección:</label></td>
                                <td><?=$userData['direccion']?></td>
                            </tr>
                            <tr>
                                <td><label id="lblEmail" class="control-label">Email:</label></td>
                                <td><?=$userData['email']?></td>
                            </tr>
                            <tr>
                                <td><label id="lblCiudad" class="control-label">Ciudad:</label></td>
                                <td><?=$userData['poblacion']?></td>
                            </tr>
                            <tr>
                                <td><label id="lblProvin" class="control-label">Provincia:</label></td>
                                <td><?=$userData['provincia']?></td>
                            </tr>
                            <tr>
                                <td><label id="lblTlf" class="control-label">Teléfono:</label></td>
                                <td><?=$userData['telefono']?></td>
                            </tr>
                            <tr>
                                <td><label id="lblCarta" class="control-label">Carta de presentación:</label></td>
                                <td><?=$userData['cartaPresentacion']?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel-footer">
                <div align="right">
                    <?php echo anchor("home/index", "Volver", array("title" => "Volver", "class" => "btn btn-primary"));?>
                </div>
            </div>
        </div>
    </main>
    <?php $this->load->view('includes/footer.php');?>
</body>
</html>