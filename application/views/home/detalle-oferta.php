<!DOCTYPE html>
<html>
<head>
    <title>Buscador de ofertas</title>
    <?php $this->load->view('includes/header.php');?>
</head>
<body style="background-color: #cccccc">
    <header class="container">
        <div class="panel-group panel-default col-md-10" >
            <?php $this->load->view('home/menu')?>
            <?php $this->load->view('home/submenu')?>
        </div>
    </header>
    <main class="container">
        <div class="panel-group panel-default col-md-10">
            <div class="panel-heading">
                <div><label id="lblTitulo"><h2><strong><?php echo $oferta['titulo'] ?></strong> </h2></label></div>
            </div>
            <div class="panel-body" style="background-color: #e6e6e6">
                <div class="col-lg-3">
                    <ul class="list-group">
                        <li><a href=""><?php echo $oferta['empresa'] ?></a></li>
                        <li><span id="lblheadPoblacion"><?php echo $oferta['ciudad'] ?></span></li>
                        <li><span id="lblheadDesde">Publicada desde <?php echo $oferta['empresaProvincia'] ?></span></li>
                    </ul>
                </div>
                <div class="col-lg-5">
                    <ul class="list-group">
                        <li><span id="lblheadSalario"><?=$oferta['salario']?> euros bruto / año</span></li>
                        <li><span id="lblheadExperiencia">Experiencia minima: <?=$oferta['reqMin']?></span></li>
                        <li><span id="lblheadTipo">Tipo oferta </span></li>
                    </ul>
                </div>
                <div class="col-lg-4">
                    <?php
                    if ($this->session->has_userdata('user-public')) {
                        if ($inscrito) {
                            echo anchor("home/desinscribirOferta/" . $oferta['id']. '/' . str_replace('/', '837599239592', $this->uri->uri_string()), "Desinscribirme de esta oferta", array("title" => "Desinscribirme", "class" => "btn btn-info", "style" => "float:right"));
                        } else {
                            echo anchor("home/inscribirOferta/" . $oferta['id'] . '/' . str_replace('/', '837599239592', $this->uri->uri_string()), "Inscribirme a esta oferta", array("title" => "Inscribirme", "class" => "btn btn-primary", "style" => "float:right"));
                        }
                    } else {
                        echo anchor("home/login", "Inscribirme", array("title" => "Inscribirme a esta oferta", "class" => "btn btn-primary", "style" => "float:right"));
                    }
                    ?>
                </div>
            </div>
        </div>

        <div class="panel-group panel-default col-md-10">
            <div class="panel-body" style="background-color: #ffffff" >
                <h3><strong>Requisitos</strong></h3>
                <br>
                <div>
                    <h5><strong>Estudios mínimos</strong></h5>
                    <h5><span id="lblEstMin"><?=$oferta['estudiosMinimos']?></span></h5>
                </div>
                <div>
                    <h5><strong>Experiencia mínima</strong></h5>
                    <h5><span id="lblExpMin"><?=$oferta['reqMin']?></span></h5>
                </div>
                <div>
                    <h5><strong>Conocimientos necesarios</strong></h5>
                    <h5><span id="lblConocim"><?=$oferta['conocimientos']?></span></h5>
                </div>
                <div>
                    <h5><strong>Requisitos mínimos</strong></h5>
                    <h5><span id="lblReqMin"><?=$oferta['reqMin']?></span></h5>
                </div>
                <br>
                <h3><strong>Descripción</strong></h3>
                <h5><span id="lblDesc"><?=$oferta['descripcion']?></span></h5>
                <br>
                <div>
                    <h5><strong>Tipo de industria de la oferta</strong></h5>
                    <h5><span id="lblDepartamento"><?=$oferta['departamento']?></span></h5>
                </div>
                <div>
                    <h5><strong>Personal a cargo</strong></h5>
                    <h5><span id="lblPersCargo"><?=$oferta['personalACargo']?></span></h5>
                </div>
                <div>
                    <h5><strong>Número de vacantes</strong></h5>
                    <h5><span id="lblVacantes"><?=$oferta['vacantes']?></span></h5>
                </div>
                <div>
                    <h5><strong>Salario</strong></h5>
                    <h5><span id="lblSalario"><?=$oferta['salario']?> euros brutos / año</span></h5>
                </div>

                <div>
                    <h6><label id="lblSalario" class="control-label"><?=$oferta['inscritos']?> inscritos a esta oferta para <?=$oferta['vacantes']?> vacantes</label></h6>
                </div>
                <div>
                    <?php echo anchor("home/listaOfertas", "Volver", array("title" => "Volver", "class" => "btn btn-primary", "style" => "float:left"))?>
                <?php
                    if ($this->session->has_userdata('user-public')) {
                        if ($inscrito) {
                            echo anchor("home/desinscribirOferta/" . $oferta['id']. '/' . str_replace('/', '837599239592', $this->uri->uri_string()), "Desinscribirme", array("title" => "Desinscribirme", "class" => "btn btn-info", "style" => "float:right"));
                        } else {
                            echo anchor("home/inscribirOferta/" . $oferta['id'] . '/' . str_replace('/', '837599239592', $this->uri->uri_string()), "Inscribirme", array("title" => "Inscribirme", "class" => "btn btn-primary", "style" => "float:right"));
                        }
                    } else {
                        echo anchor("home/login", "Inscribirme", array("title" => "Inscribirme", "class" => "btn btn-primary", "style" => "float:right"));
                    }
                ?>
                </div>
            </div>
        </div>
    </main>
</body>
<?php $this->load->view('includes/footer.php');?>
</html>