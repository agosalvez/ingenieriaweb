<!DOCTYPE html>
<html>
<head>
	<title>Registro</title>
    <?php $this->load->view('includes/header.php');?>
</head>
<body>
    <header>
        <?php
        if (isset($msg)) {
            ?>
            <div class="msg-register-<?php echo $type ?> alert alert-<?php echo $type;?>" data-url="<?php echo base_url() ?>index.php/home/login"><?php echo $msg;?></div>
            <?php
        }
        ?>
    </header>
	<main class="container">
		<div class="panel-group panel-primary ">
            <a href="<?php echo base_url() ?>index.php/home/index" class="btn btn-default">Volver</a>
            <div class="panel-heading" align="center">Formulario de Alta</div>
            <div class="panel-body" style="background-color: #f5f5f0" >
                <form id="formRegister" action="<?php echo base_url() ?>index.php/home/registroAction" method="POST">
                    <div class="form-group" id="nicknameForm">
                        <div class="row">
                            <label for="dni" class="control-label col-md-2">Dni (*)</label>
                            <div class="col-sm-4">
                            <input type="text" name="dni" id="dni" maxlength="20" required class="col-md-4 form-control" placeholder="Dni">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="nombre" class="control-label col-md-2">Nombre (*)</label>
                            <div class="col-sm-4">
                                <input type="text" name="nombre" id="nombre" maxlength="100" required class="col-md-4 form-control" placeholder="Nombre">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="apellidos" class="control-label col-md-2">Apellidos (*)</label>
                            <div class="col-sm-4">
                                <input type="text" name="apellidos" id="apellidos" required class="col-md-4 form-control" maxlength="250" placeholder="Apellidos">
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="emailForm">
                        <div class="row">
                            <label for="email" class="control-label col-md-2">Email(*)</label>
                            <div class="col-sm-4" >
                                <input type="email" name="email" id="email" required class="form-control" maxlength="100" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="passForm">
                        <div class="row">
                            <label for="pass" class="control-label col-md-2">Password(*)</label>
                            <div class="col-sm-4">
                            <input type="Password" name="password" id="pass" required class="col-md-4 form-control" minlength="6" placeholder="Password" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="confirmPass" class="control-label col-md-2">Confirmar Passowrd(*)</label>
                            <div class="col-sm-4">
                            <input type="Password" name="password2" id="confirmPass" required class="col-md-4 form-control" minlength="6" placeholder="Confirm Passowrd">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="direccion" class="control-label col-md-2">Dirección (*)</label>
                            <div class="col-sm-4">
                                <input type="text" name="direccion" id="direccion" required class="col-md-4 form-control" placeholder="Dirección">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="ciudad" class="control-label col-md-2">Ciudad</label>
                            <div class="col-sm-4">
                            <select name="poblacion" class="col-md-4 btn btn-default form-control">
                                <option value="-1">...Selecciona una ciudad...</option>
                                <option value="Alicante">Alicante</option>
                                <option value="Elche">Elche</option>
                                <option value="Ibi">Ibi</option>
                                <option value="San Juan">San Juan</option>
                                <option value="San Vicente">San Vicente</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="provincia" class="col-md-2 control-label">Provincia</label>
                            <div class="col-sm-4">
                            <select name="provincia" class="col-md-4 btn btn-default form-control">
                                <option value="-1">Seleccione una provincia</option>
                                <?php
                                    for ($i = 0; $i < count($localidades); ++$i) {
                                        echo '<option value="' . $localidades[$i]->nombre . '">' . $localidades[$i]->nombre . '</option>';
                                    }
                                ?>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="paises" class="col-md-2 control-label">País</label>
                            <div class="col-sm-4">
                                <select name="pais" class="col-md-4 btn btn-default form-control">
                                    <option value="-1">Seleccione un país</option>
                                    <?php
                                    for ($i = 0; $i < count($paises); ++$i) {
                                        echo '<option value="' . $paises[$i]->nombre . '">' . $paises[$i]->nombre . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="sexo" class="control-label col-md-2">Género</label>
                            <div class="col-sm-4">
                                <input type="radio" name="genero" checked value="0" id="sexoM">Mujer
                                <input type="radio" name="genero" value="1" id="sexoH">Hombre
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="fechaNac" class="control-label col-md-2">Fecha Nacimiento</label>
                            <div class="col-sm-4">
                                <input type="date" name="dob" id="fechaNac" class="col-md-4 form-control" min="1910-01-02" max="1998-12-31">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="tlf1" class="control-label col-md-2">Teléfono 1</label>
                            <div class="col-sm-4">
                            <input type="text" name="tlf1" id="tlf1" class="col-md-4 form-control" maxlength="9" placeholder="Teléfono 1">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="tlf2" class="control-label col-md-2">Teléfono 2</label>
                            <div class="col-sm-4">
                            <input type="text" name="tlf2" id="tlf2" class="col-md-4 form-control" maxlength="9" placeholder="Teléfono 2">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="exp" class="control-label col-md-2">Años de experiencia</label>
                            <div class="col-sm-4">
                                <input type="number" name="exp" id="exp" class="col-md-4 form-control" maxlength="9" placeholder="Años de experiencia">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="carta" class="control-label col-md-2">Carta de presentación</label>
                            <div class="col-sm-4">
                                <textarea cols="6" rows="7" name="carta" id="carta" class="col-md-4 form-control" placeholder="Carta de presentación"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class=" col-sm-4">
                            <input class="btn btn-primary col-md-4 form-control"  type="submit"  value="Enviar" id="btnEnviar">
                        </div>
                        <div class="col-sm-2">
                            <?php echo anchor("home/index", "Cancelar", array("title" => "Cancelar", "class" => "btn btn-danger col-md-2 col-md-offset-1 form-control"));?>
                        </div>
                    </div>
                </form>
			</div>
		<div class="form-group" >
			<div id="alerta"></div>	
		</div>	
	</main>
    <?php $this->load->view('includes/footer.php');?>
</body>
</html>
