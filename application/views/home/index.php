<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
    <title>InfoJobs - Bolsa de trabajo, ofertas de empleo</title>
    <?php $this->load->view('includes/header.php');?>
</head>
<body>
    <header class="container">
        <div class="panel-group panel-default col-md-10" >
            <?php $this->load->view('home/menu')?>
        </div>
    </header>
    <main class="container" >
        <div class="panel-group panel-primary col-md-10">
            <?php $this->load->view('home/filtros-superiores');?>
            <div class="panel-body">
                <div class="row">
                    <img width="100%" src="<?php echo base_url() ?>assets/images/empresas.png">
                    <img width="100%" src="<?php echo base_url() ?>assets/images/footer.png">
                </div>
            </div>
        </div>
    </main>
</body>
<?php $this->load->view('includes/footer.php');?>
</html>
