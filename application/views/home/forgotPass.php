<!DOCTYPE html>
<html>
<head>
	<title>Recuperación de contraseña</title>
	<?php $this->load->view('includes/header.php');?>
</head>
<body>
	<header></header>
	<main class="container">
		<div class="panel-group panel-primary col-md-7">
			<div class="panel-heading">Formulario de recuperación de contraseña</div>
			<div class="panel-body" style="background-color: #f5f5f0">
				<p>Se le enviará un email a su cuenta con instrucciones para reestablecer la contraseña.</p>
				<div class="form-group">
                    <form action="" method="POST" id="">
					    <label for="email" class="control-label">Introduzca un email válido:</label>
					    <input type="email" name="email" id="email" class="form-control" placeholder="Email" required>
                    </form>
				</div>
				<div class="form-group">
					<input type="submit" id="btnEnviarRecupPass" class="btn btn-primary col-md-2" value="Enviar">
                    <?php echo anchor("home/login", "Volver", array("title" => "Volver", "class" => "btn btn-default col-md-2 col-md-offset-1"));?>
				</div>
			</div>			
		</div>		
	</main>
	<?php $this->load->view('includes/footer.php');?>
</body>
</html>