<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
    <title>Buscador de ofertas</title>
    <?php $this->load->view('includes/header.php');?>
</head>
<body>
<header class="container">
    <div class="panel-group panel-default col-md-10" >
        <?php $this->load->view('home/menu')?>
        <?php $this->load->view('home/submenu')?>
    </div>
</header>
<main class="container" >
    <div class="panel-group panel-primary col-md-10">
        <div class="panel-body">
            <div class="row">
                <h2>Subir CV</h2>
                <form action="<?=base_url("index.php/home/subircvAction")?>" method="POST" enctype="multipart/form-data">
                    <input type="file" name="userfile" value="fichero"/>
                    <input type="submit" value="Enviar"/>
                </form>
                <?php
                if (isset($error)) {
                    echo "<strong style='color:red;'>".$error."</strong>";
                } else if (isset($img)) {
                    echo "<strong style='color:green;'>".$img["orig_name"]." subido satisfactoriamente </strong>";
                }
                ?>
            </div>
        </div>
        <?php echo anchor("home/index", "Volver", array("title" => "Volver", "class" => "btn btn-primary")); ?>
    </div>
</main>
</body>
<?php $this->load->view('includes/footer.php');?>
</html>

