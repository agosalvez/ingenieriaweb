<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
    <title>Buscador de ofertas</title>
    <?php $this->load->view('includes/header.php');?>
</head>
<body>
    <header class="container">
        <div class="panel-group panel-default col-md-10" >
        <?php $this->load->view('home/menu')?>
        <?php
            if ($this->session->has_userdata('user-public')) {
                $this->load->view('home/submenu');
            }
        ?>
      </div>
    </header>
    <main class="container" >
        <div class="panel-group panel-primary col-md-10">
            <?php $this->load->view('home/filtros-superiores');?>
            <div class="panel-body">
                <div class="row">
                    <?php $this->load->view('home/lista-ofertas')?>
                </div>
            </div>
        </div>
    </main>
</body>
<?php $this->load->view('includes/footer.php');?>
</html>
