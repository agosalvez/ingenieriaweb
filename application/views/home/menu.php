<div class="panel-heading">
    <div class="row">
        <div class="col-xs-2">
            <a href="<?php echo base_url() . 'index.php/home/index'?>"><img src="<?php echo base_url() ?>assets/images/infojobs.png"></a>
        </div>
        <div class="col-xs-5">
            <?php echo anchor("home/index", "Empleo", array("title" => "Empleo", "class" => "menu-level-1"));?>
            <?php echo anchor("home/index", "Freelance", array("title" => "Freelance", "class" => "menu-level-1"));?>
            <?php echo anchor("home/index", "Executive", array("title" => "Executive", "class" => "menu-level-1"));?>
        </div>
        <div class="col-xs-5">
            <div class="row" <?php echo ($this->session->has_userdata('user-public')) ? 'style="float: right; margin: 0 30px 0 0;"' : ''?>>
                <?php if ($this->session->has_userdata('user-public')) { ?>
                    <div><span>Bienvenido, <?=$this->session->userdata('user-public')?></span>
                    <span><?php echo anchor("home/logout", "Logout", array("title" => "Logout", "class" => "btn btn-danger"));?></span></div>
                <?php } else {
                    echo anchor("empresa/index", "Zona empresa", array("title" => "Zona empresa", "class" => "btn btn-warning"));
                    echo anchor("home/login", "Acceder", array("title" => "Accede", "class" => "btn btn-default", "style" => "left: 15px; position: relative;"));
                    echo anchor("home/registro", "Date de alta", array("title" => "Date de alta", "class" => "btn btn-primary", "style" => "left: 30px; position: relative;"));
                } ?>
            </div>
        </div>
    </div>
</div>