<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="panel-heading">
    <form action="<?php echo base_url() ?>index.php/home/listaOfertas" method="POST" id="formBuscar">
        <div class="row">
            <div class="col-xs-12">Buscar ofertas de...  </div>
        </div>
        <div class="row">
            <div class="col-xs-10">
                <div class="input-group col-md-12">
                    <input type="text" name="buscar" class="form-control" placeholder="Puesto, empresa o palabra clave">
                </div>
            </div>
            <div class="col-lg-2">
                <input type="submit" class="btn btn-primary col-md-10" value="Buscar" style="background-color:#004466">
            </div>
        </div>
    </form>
</div>