<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
    <title>Login empresas</title>
    <?php $this->load->view('includes/header.php');?>
</head>
<body>
    <header class="container">
        <?php
            if (isset($msg)) {
        ?>
            <div class="alert alert-<?php echo $type;?>"><?php echo $msg;?></div>
        <?php
            }
        ?>
        <div class="panel-group panel-default col-md-offset-3 col-md-6">
        <div class="panel-heading">
            <?php echo anchor('home/index', '<img src="'.base_url().'assets/images/IJ-logo.png" alt="Principal" />'); ?>
        </div>
        </div>
    </header>

    <main class="container">
        <div class="panel-group panel-primary col-md-offset-3 col-md-6">
            <div class="panel-heading" align="center">Formulario de Login - Empresas</div>
            <div class="panel-body" style="background-color: #f5f5f0">
            <form method="POST" id="loginform" action="<?php echo base_url() . 'index.php/empresa/oferta';?>">
                <div class="form-group" >
                    <input type="text" id="username" name="username" maxlength="20" required placeholder="Username" class="form-control">
                </div>
                <div class="form-group" >
                    <input type="password" id="password" name="password" required placeholder="Password" class="form-control">
                </div>
                <div class="form-group">
                    <input type="submit" id="btnLogin" class="btn btn-warning form-control" value="Login">
                    <?php echo anchor("home/index", "Volver", array("title" => "Volver", "class" => "btn btn-default col-xs-12", "style" => "margin-top: 10px"));?>
                </div>
            </form>
            </div>
            <div class="col-md-12 panel-footer" style="background-color: #ccccff">
                ¿Olvidaste el Password?
            </div>

        </div>
    </main>

</body>
<?php $this->load->view('includes/footer.php');?>
</html>
