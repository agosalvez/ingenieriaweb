<?php defined('BASEPATH') OR exit('No direct script access allowed');
// Compruebo si está logado el usuario en la sesión
if ($this->session->has_userdata('user-empresa')) { ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>Gestión Empresa - Ofertas</title>
        <?php $this->load->view('includes/header.php');?>
    </head>
    <body>
    <header class="container">
    <?php
        $this->load->view('empresa/menu-empresa');
    ?>
    </header>
    <main class="container">
    <?php
        echo $output;
    ?>
    </main>
    </body>
    <?php $this->load->view('includes/footer.php');?>
    </html>
<?php
} else {
    $this->load->view('empresa/login');
}
