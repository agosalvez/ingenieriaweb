<nav class="navbar navbar-default" role="navigation">
    <!-- El logotipo y el icono que despliega el menú se agrupan
         para mostrarlos mejor en los dispositivos móviles -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target=".navbar-ex1-collapse">
            <span class="sr-only">Desplegar navegación</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <?php echo anchor('', '<img class="logo-bo" src="' . base_url() . 'assets/images/IJ-logo.png' . '" title="Infojobs">')?>

    </div>

    <!-- Agrupar los enlaces de navegación, los formularios y cualquier
         otro elemento que se pueda ocultar al minimizar la barra -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
            <li><?php echo anchor('empresa/oferta', 'Oferta')?></li>
            <li><?php echo anchor('empresa/candidatos', 'Candidatos')?></li>
            <li><?php echo anchor('empresa/logout', 'Logout', array("title" => "Cerrar sesión"))?></li>
        </ul>
    </div>
</nav>