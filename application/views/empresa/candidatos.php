<?php defined('BASEPATH') OR exit('No direct script access allowed');
// Compruebo si está logado el usuario en la sesión
if ($this->session->has_userdata('user-empresa')) { ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>Gestión Empresa - Candidatos</title>
        <?php $this->load->view('includes/header.php');?>
    </head>
    <body>
    <header class="container">
        <?php
        $this->load->view('empresa/menu-empresa');
        ?>
    </header>
    <main class="container">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Oferta</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Dirección</th>
                <th>Ciudad</th>
                <th>Provincia</th>
                <th>Años de experiencia</th>
                <th>¿Carta de presentación?</th>
                <th>CV</th>
                <th colspan="2">¿Qué desea hacer?</th>
            </tr>
            </thead>
            <tbody>
            <?php
            for ($i = 0; $i < count($candidatos); ++$i) {
                ?>
                <tr class="child-<?php echo $candidatos[$i]['idT'] . $candidatos[$i]['idO']?>">
                    <th scope="row"><?php echo $i+1 ?></th>
                    <td><?php echo $candidatos[$i]['idO'] . ' - ' . $candidatos[$i]['titulo']?></td>
                    <td><?php echo $candidatos[$i]['nombre'] . ' ' . $candidatos[$i]['apellidos']?></td>
                    <td><?php echo $candidatos[$i]['email']?></td>
                    <td><?php echo $candidatos[$i]['direccion']?></td>
                    <td><?php echo $candidatos[$i]['poblacion']?></td>
                    <td><?php echo $candidatos[$i]['provincia']?></td>
                    <td><?php echo $candidatos[$i]['anyosExperiencia']?></td>
                    <td><?php echo (strlen($candidatos[$i]['cartaPresentacion']) > 10) ? 'Si' : 'No'?></td>
                    <?php
                    if ($candidatos[$i]['cv'] == 1) { ?>
                        <td><a class="btn btn-info btn-xs" href="<?php echo base_url() . 'index.php/empresa/bajarcv/' . $candidatos[$i]['id']?>" title="Ver candidato">Descargar</a></td>
                    <?php } else { ?>
                        <td>No</td>
                    <?php }
                    ?>
                    <td><a class="btn btn-success btn-xs acceptCandidate" data-trabajador="<?php echo $candidatos[$i]['idT']?>" data-oferta="<?php echo $candidatos[$i]['idO']?>" href="javascript:;" title="Aceptar candidatura">Aceptar</a></td>
                    <td><a class="btn btn-danger btn-xs refuseCandidate" data-trabajador="<?php echo $candidatos[$i]['idT']?>" data-oferta="<?php echo $candidatos[$i]['idO']?>" href="javascript:;" title="Rechazar candidatura">Rechazar</a></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <input class="url-ajax" type="hidden" value="<?php echo base_url(); ?>index.php/">
    </main>
    </body>
    <?php $this->load->view('includes/footer.php');?>
    </html>
    <?php
} else {
    $this->load->view('empresa/login');
}
