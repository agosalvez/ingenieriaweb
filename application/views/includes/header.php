	<meta charset="utf-8">
	<meta http-equiv="content-type" content="text/html" charset="utf-8">

	<link rel="icon" href="http://media.infojobs.net/appgrade/icons/ico-favicon-ij-192x192.png" sizes="192x192" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/bootstrap.min.css'?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/bootstrap-theme.min.css'?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/style.css'?>">

	<script type="text/javascript" src="<?php echo base_url() . 'assets/js/jquery-3.1.1.min.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url() . 'assets/js/bootstrap.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url() . 'assets/js/scripts.js'?>"></script>
    <!-- Grocery crud -->
    <?php
        if (isset($css_files)) {
            foreach($css_files as $file): ?>
                <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
            <?php endforeach;
        }
        if (isset($js_files)) {
            foreach($js_files as $file): ?>
                <script src="<?php echo $file; ?>"></script>
            <?php endforeach;
        }
