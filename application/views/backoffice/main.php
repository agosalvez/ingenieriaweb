<?php defined('BASEPATH') OR exit('No direct script access allowed');
// Compruebo si está logado el usuario en la sesión
if ($this->session->has_userdata('user-backoffice')) { ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>Gestión Backoffice</title>
        <?php $this->load->view('includes/header.php');?>
    </head>
    <body>
    <header class="container">
    <?php
        $this->load->view('backoffice/menu-bo');
    ?>
    </header>
    <main class="container">
    <?php
        $this->load->view('backoffice/vista-crud', $output);
    ?>
    </main>
    </body>
    <?php $this->load->view('includes/footer.php');?>
    </html>
<?php
} else {
    $this->load->view('backoffice/login');
}
