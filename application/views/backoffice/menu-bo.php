<nav class="navbar navbar-default" role="navigation">
    <!-- El logotipo y el icono que despliega el menú se agrupan
         para mostrarlos mejor en los dispositivos móviles -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target=".navbar-ex1-collapse">
            <span class="sr-only">Desplegar navegación</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <?php echo anchor('', '<img class="logo-bo" src="' . base_url() . 'assets/images/IJ-logo.png' . '" title="Infojobs">')?>

    </div>

    <!-- Agrupar los enlaces de navegación, los formularios y cualquier
         otro elemento que se pueda ocultar al minimizar la barra -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
            <li><?php echo anchor('backoffice/renderCrud/localidades', 'Localidades')?></li>
            <li><?php echo anchor('backoffice/renderCrud/paises', 'Países')?></li>
            <li><?php echo anchor('backoffice/renderCrud/idiomas', 'Idiomas')?></li>
            <li><?php echo anchor('backoffice/renderCrud/categoria-empresa', 'Categorias de empresas')?></li>
            <li><?php echo anchor('backoffice/renderCrud/oferta', 'Ofertas')?></li>
            <li><?php echo anchor('backoffice/renderCrud/trabajador', 'Trabajadores')?></li>
            <li><?php echo anchor('backoffice/renderCrud/empresa', 'Empresas')?></li>
            <li><?php echo anchor('backoffice/renderCrud/cv', 'Cv\'s')?></li>
            <li><?php echo anchor('backoffice/logout', 'Logout', array("title" => "Cerrar sesión"))?></li>
        </ul>
    </div>
</nav>