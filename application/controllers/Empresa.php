<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('empresa_model');
        $this->load->library('grocery_CRUD'); // Cargamos libreria grocery (libreria externa o de terceros)
    }

	public function index() {
        if ($this->session->has_userdata('user-empresa')) {
            $this->oferta();
        } else {
            $this->login();
        }
	}

	/*
	 * Muestra la vista principal de login para la empresa
	 */
	public function login() {
		$this->load->view('empresa/login');
	}

	/*
	 * Loguea al usuario, recupera datos del formulario de login y los contrasta con la bbdd
	 */
	public function doLogin() {
        // Los agrupo en un array
		$loginData = array("user" => $_POST['username'],
						"password" => $_POST['password']);

		// Llamo a la funcion doLogin del modelo para comprobar el user y pass

		$login = $this->empresa_model->doLogin($loginData);

		if ($login) { // Seteo la sesión y redirijo al main de la empresa
            $this->session->set_userdata('user-empresa', $loginData['user']);
            $this->oferta();
        } else { // Redirijo al login con mensaje de error
            $data = array(
                'msg' => 'Error al hacer login',
                'type' => 'danger'
            );
            $this->load->view('empresa/login', $data);
        }
	}

    /*
     * Desloguea al usuario en cuestión
     */
    public function logout() {
        $this->session->unset_userdata('user-empresa');
        $this->load->view('home/index');
    }

    /*
     * Función que muestra la vista principal de la empresa una vez logado
     */
	public function main() {
		$this->load->view('empresa/main');
	}

	/*
	 * Función que devuelve el id de una empresa a partir de su username (dni).
	 */
	public function getIdEmpresa() {
        return $this->empresa_model->getIdEmpresa($this->session->userdata['user-empresa']);
    }

    /*
     * Muestra la lista de ofertas de la empresa, si se está logado previamente, sino, loguea al user
     */
	public function oferta() {
	    if (!$this->session->has_userdata('user-empresa')) {
	        $loginData = array(
                "user" => $_POST['username'],
                "password" => $_POST['password']);
            $this->doLogin($loginData);
        } else {
            $crud = new grocery_CRUD();
            $crud->set_table('oferta');
            $crud->where('idEmpresa',$this->getIdEmpresa());
            $crud->order_by('id', 'desc');
            $crud->unset_edit();
            $crud->set_language('spanish');
            $crud->set_subject('oferta');
            $crud->set_theme('flexigrid');

            $output = $crud->render();
            $this->load->view('empresa/ofertas', $output);
        }
    }

    /*
     * Muestra la lista de candidatos sobre cada oferta publicada de la empresa
     */
    public function candidatos() {
        $name               = $this->session->userdata['user-empresa'];
        $idEmpresa          = $this->empresa_model->getIdEmpresa($name);
        $data['candidatos'] = $this->empresa_model->getCandidatos($idEmpresa);

        $this->load->view('empresa/candidatos', $data);
    }

    public function gestionarCandidato() {
        $data['idTrabajador'] = $this->input->post('idTrabajador');
        $data['idOferta'] = $this->input->post('idOferta');
        $data['type'] = $this->input->post('type');
        if ($this->empresa_model->setCandidatura($data)) {
            echo intval($data['idTrabajador'] . $data['idOferta']);
        } else {
            echo -1;
        }
    }

    public function bajarcv($idUser) {
        $ext = '.pdf';
        $filename = dirname(__DIR__) . '/subidas/' . $idUser . $ext;
        if (file_exists($filename)) {
            $this->load->helper(array('download', 'file'));
            $obj        = new private_model();
            $userData   = $obj->getUserDataFromId($idUser);
            $name       = 'Cv-' . $userData['nombre'] . ' ' . $userData['apellidos'] . $ext;
            $name       = str_replace(' ', '-', $name);
            $data       = file_get_contents($filename);
            force_download($name, $data);
        }
        $this->candidatos();
    }
}
