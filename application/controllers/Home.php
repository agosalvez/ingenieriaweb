<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->model('public_model');
    }

    /*
     * Función de entrada principal del controlador
     */
	public function index() {
        if (!$this->session->has_userdata('user-public')) {
            $this->load->view('home/index');
        } else {
            $this->session->unset_userdata('buscar');
            $this->listaOfertas();
        }
	}

    /*
     * Muestra la vista principal de login para el backoffice
     */
    public function login() {
        if (!$this->session->has_userdata('user-public')) {
                $this->load->view('home/login');
        } else {
            redirect('/home', 'location');
        }
    }

    /*
     * Loguea al usuario, recupera datos del formulario de login y los contrasta con la bbdd
     */
    public function doLogin() {
        // Recojo los datos del formulario de login
        $email      = $this->input->post('email');
        $password   = $this->input->post('password');

        // Los agrupo en un array
        $loginData = array("email" => $email,
                            "password" => $password);

        // Llamo a la funcion doLogin del modelo para comprobar el email y password
        $login = $this->public_model->doLogin($loginData);

        if ($login) { // Seteo la sesión y redirijo al main del home
            $this->session->set_userdata('user-public', $loginData['email']);
            redirect('/home', 'location');
        } else { // Redirijo al login con mensaje de error
            $data = array(
                'msg' => 'Error al hacer login',
                'type' => 'danger'
            );
            $this->load->view('home/login', $data);
        }
    }

    /*
     * Desloguea al usuario en cuestión de la parte pública
     */
    public function logout() {
        $this->session->unset_userdata('user-public');
        $this->index();
    }

    /*
     * Carga la vista de recordar contraseña
     */
    public function forgotPass() {
        if (!$this->session->has_userdata('user-public')) {
            $this->load->view('home/forgotPass');
        } else {
            $this->index();
        }
    }

    /*
     * Carga los datos y la vista de registro de usuario
     */
    public function registro() {
        if (!$this->session->has_userdata('user-public')) {
            $data['localidades'] = $this->public_model->getAllData('localidades');
            $data['paises']      = $this->public_model->getAllData('paises');
            $this->load->view('home/registro', $data);
        } else {
            redirect('/home', 'location');
        }
    }

    /*
     * Registra al usuario nuevo
     */
    public function registroAction() {
        $data = array(
            'dni'               => $this->input->post('dni'),
            'nombre'            => $this->input->post('nombre'),
            'apellidos'         => $this->input->post('apellidos'),
            'genero'            => intval($this->input->post('genero')),
            'direccion'         => $this->input->post('direccion'),
            'telefono'          => $this->input->post('telefono'),
            'telefono2'         => $this->input->post('telefono2'),
            'poblacion'         => $this->input->post('poblacion'),
            'provincia'         => $this->input->post('provincia'),
            'pais'              => $this->input->post('pais'),
            'email'             => $this->input->post('email'),
            'dob'               => $this->input->post('dob'),
            'anyosExperiencia'  => $this->input->post('exp'),
            'password'          => md5($this->input->post('password')),
            'cartaPresentacion' => $this->input->post('carta'),
        );
        // SI ES CORRECTO MUESTRO MENSAJES DE OK O DE ERROR
        if ($this->public_model->registro($data)) {
            $this->load->view('home/login', array('msg'=>'Registro compleado correctamente','type'=>'success'));
        } else {
            $this->load->view('home/registro', array('msg'=>'Error en el registro, por favor, revise los campos','type'=>'danger'));
        }
    }


    public function ajustes() {
        if ($this->session->has_userdata('user-public')) {
            $emailUser          = $this->session->userdata('user-public');
            $data['userData']   = $this->private_model->getUserData($emailUser);
            $this->load->view('home/ajustes', $data);
        } else {
            $this->load->view('home/index');
        }
    }

    /*
     * Lista las ofertas, teniendo en cuenta si se busca algo en el buscador
     */
    public function listaOfertas() {
        if (isset($_POST['buscar'])) { // al buscar
            $busqueda = $_POST['buscar'];
            $this->session->set_userdata('buscar', $_POST['buscar']);
        } else { // al paginar
            $busqueda = $this->session->userdata('buscar');
        }
        $per_page = 2;
        $this->preparePagination($per_page);
        $data['ofertas'] = $this->public_model->getAllOfertas($per_page, $busqueda);

        // Pedir datos de usuario
        $emailUser          = $this->session->userdata('user-public');
        $ofertasInscritas   = $this->private_model->getOffersSigned($emailUser);
        // Recortamos la descripción para la vista previa
        $data               = recortarDescripcion($data, 200);
        // Para checkear si está inscrito o no a una oferta el usuario logado
        $data['ofertas']    = checkOffersSignedButton($data['ofertas'], $ofertasInscritas);

        // paso todas las ofertas del modelo con un per_page (numero de noticias por pagina)
        // creo una variable que crea los links para la paginación
        $data = array('ofertas' => $data['ofertas'],
            'paginacion' => $this->pagination->create_links());

        $this->load->view('home/buscador', $data);
    }

    /*
     * Prepara la configuración para la paginación
     */
    public function preparePagination($per_page) {
        // Configuración de la paginación
        $config['base_url']         = base_url().'index.php/home/listaOfertas/';
        $config['total_rows']       = $this->public_model->numOfertas();
        $config['per_page']         = $per_page;
        $config['num_links']        = 5;
        $config['first_link']       = 'Primero ';
        $config['last_link']        = ' Ultimo ';
        $config['next_link']        = ' Siguiente ';
        $config['prev_link']        = ' Anterior ';
        $config['cur_tag_open']     = '<a href="javascript:;" class="active">';
        $config['cur_tag_close']    = '</a>';
        $config['full_tag_open']    = '<div class="pagination">';
        $config['full_tag_close']   = '</div>';
        // Inicializo la paginación con los parámetros de $config
        $this->pagination->initialize($config);
    }

    public function getUserId() {
        $emailUser  = $this->session->userdata('user-public');
        return $this->private_model->getIdUserFromEmail($emailUser);
    }

    /*
     * Inscribe al usuario logado de la oferta clicada
     */
    public function inscribirOferta($idOferta, $url) {
        $idUser     = $this->getUserId();
        $aux        = str_replace('837599239592','/',$url) . '/';
        $this->private_model->inscribirUsuarioAOferta($idOferta, $idUser);
        redirect($aux, 'location');
    }

    /*
     * Desinscribe al usuario logado de la oferta clicada
     */
    public function desinscribirOferta($idOferta, $url) {
        $idUser     = $this->getUserId();
        $aux        = str_replace('837599239592','/',$url) . '/';
        $this->private_model->desinscribirUsuarioAOferta($idOferta, $idUser);
        redirect($aux, 'location');
    }

    public function detalleOferta($idOferta) {
        $userId = $this->getUserId();
        if ($userId > 0) {
            $data['inscrito'] = $this->private_model->checkUserSignedOferta($idOferta, $userId);
        }
        $data['oferta'] = $this->public_model->getOferta($idOferta);
        $this->load->view('home/detalle-oferta', $data);
    }

    public function subircv() {
        $this->load->view('home/subircv');
    }

    public function subircvAction(){
        //Ruta donde se guardan los ficheros
        $config['upload_path'] = './application/subidas/';

        //Modifico el nombre del fichero para que sea único
        $config['file_name'] = $this->getUserId();

        //Tipos de ficheros permitidos
        $config['allowed_types'] = 'pdf';

        $file = $config['upload_path'] . $config['file_name'] . '.pdf';

        if (file_exists($file)) {
            unlink($file);
        }

        //Se pueden configurar aun mas parámetros.
        //Cargamos la librería de subida y le pasamos la configuración
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload()){
            /*Si al subirse hay algún error lo meto en un array para pasárselo a la vista*/
            $error=array('error' => $this->upload->display_errors());
            $this->load->view('home/subircv', $error);
        }else{
            //Actualizamos la usuario con la subida de cv
            $this->public_model->userUploadCv($this->getUserId());

            //Datos del fichero subido
            $datos["img"]=$this->upload->data();

            // Podemos acceder a todas las propiedades del fichero subido
            // $datos["img"]["file_name"]);

            //Cargamos la vista y le pasamos los datos
            $this->load->view('home/subircv', $datos);
        }
    }
}

