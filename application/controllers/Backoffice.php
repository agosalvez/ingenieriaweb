<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backoffice extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD'); // Cargamos libreria grocery (libreria externa o de terceros)
    }

	public function index() {
		$this->login();
	}

	/*
	 * Muestra la vista principal de login para el backoffice
	 */
	public function login() {
		$this->load->view('backoffice/login');
	}

	/*
	 * Loguea al usuario, recupera datos del formulario de login y los contrasta con la bbdd
	 */
	public function doLogin() {
        // Recojo los datos del formulario de login
		$username = $_POST['username'];
		$password = $_POST['password'];

        // Los agrupo en un array
		$loginData = array("user" => $username,
						"password" => $password);

		// Llamo a la funcion doLogin del modelo para comprobar el user y pass
		$this->load->model('backoffice_model');
		$login = $this->backoffice_model->doLogin($loginData);

		if ($login) { // Seteo la sesión y redirijo al main del backoffice
            $this->session->set_userdata('user-backoffice', $loginData['user']);
            $this->renderCRUD('localidades');
        } else { // Redirijo al login con mensaje de error
            $data = array(
                'msg' => 'Error al hacer login',
                'type' => 'danger'
            );
            $this->load->view('backoffice/login', $data);
        }
	}

    /*
     * Desloguea al usuario en cuestión
     */
    public function logout() {
        $this->session->unset_userdata('user-backoffice');
        $this->load->view('home/index');
    }

    /*
     * Función que muestra la vista principal del Backoffice una vez logado
     */
	public function main() {
		$this->load->view('backoffice/main');
	}

	public function renderCRUD($table) {
        $crud = new grocery_CRUD();
        $crud->set_table($table);
        $crud->set_subject(ucfirst($table));
        $crud->set_theme('flexigrid');

        $output = $crud->render();

        $this->load->view('backoffice/main', $output);
    }
}
